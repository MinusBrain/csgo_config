# CS:GO Config Tipps

## **autoexec.cfg einrichten**

Um besser mit einer eigenen Konfiguration arbeiten zu können sollte man sich ein Configfile anlegen, das jedes mal beim Öffnen von CS:GO automatisch ausgeführt wird.
**Wichtig:** Wenn ihr der Anweisung hier folgt werden bei jedem Start von CS:GO evtl. Änderungen die Ihr im Spiel macht (bspw. Tastenbelegung) von dem Configfile überschrieben. D.h. wenn Ihr im Configfile springen auf Leertaste legt und dann im Spiel im Menü auf einer andere Taste, ist es beim nächsten Neustart von CS wieder auf Leertaste.

### Leeres Configfile erzeugen
Erzeugt bitte ein File mit dem Namen autoexec.cfg in folgendem Verzeichniss:

    <steam pfad>\userdata\<userid>\730\local\cfg

Sollten bei Euch mehrere User IDs existieren (nur dann) müsst Ihr herausfinden welches Euer Account ist.
Am verlässlichsten geht das so:
1. Klickt in Steam auf Euren Namen oben und anschließend auf Profil
2. Kopiert die URL die eine der beiden folgenden Formen haben sollte
    1. https://steamcommunity.com/id/minusbrain/
    2. https://steamcommunity.com/profiles/76561197969017412
3. Geht auf https://steamidfinder.com
4. Fügt die kopierte URL von oben ins Eingabefeld auf der Webseite ein und klickt auf "Get SteamID"
5. Die längere Zahl hinter steamID3 sollte Eure User ID sein und Ihr solltet einen Ordner entsprechenden Ordner unter <steam pfad>\userdata\ finden

Bei mir ist die autoexec.cfg beispielsweise hier:

    C:\Program Files (x86)\Steam\userdata\8751684\730\local\cfg

Im ersten Schritt sollte in der dort neu erstellten autoexec.cfg Datei nur folgendes stehen:

    developer "1"
    echo "############################################# HALLO WELT #############################################"

### CS:GO Startoptionen anpassen

1. Im Steam rechte Maustaste auf Counter-Strike: Global Offensive
2. Einstellungen / Properties
3. Startoptionen / Launch Options
4. Dort dann -console hinzufügen, falls es nicht schon dort steht. Bei mir steht bspw.:

        -tickrate 128 -console -novid
    Die anderen Optionen könnt ihr im Moment ignorieren.
4. OK klicken und Properties Menü schließen
5. CS:GO starten

### Funktionierts?

Nach dem Starten sollte automatisch die Konsole aufgehen und irgendwo sollte der Text 

    ############################################# HALLO WELT #############################################
    
zu lesen sein. Falls nicht bitte nochmal alle Schritte prüfen.

## **Grundeinstellungen**

Empfohlen:
Die folgenden Grundeinstellungen sind zu empfehlen. Falls Ihr irgendwas nicht wollt schmeisst die entsprechende Zeile einfach raus.
```
cl_forcepreload "1"  // Alle Ressourcen vor dem Starten einer Map laden. Verhindert Verzoegerungen beim Spielen
net_graph "1"        // FPS / UPS und andere Performance Infos anzeigen
net_graphproportionalfont "0" // Passt die Schriftgroesse der net graph Infos Eurer Aufloesung an
cl_autowepswitch "0" // Autowechseln der Waffen beim Einsammeln ausschalten
voice_enable "1"     // Voice Chat an
developer "1"        // Entwicklerkonsole nach starten von CS:GO offen
```

## **Radareinstellungen**

Empfohlen:
Viele scheinen die Standardeinstellungen des Radars zu verwenden. Zumindest den Zoom solltet Ihr anpassen um immer die ganze Map sehen zu können. Hier sind die Einstellungen die ich nutze.
```
cl_radar_always_centered "1"           // Eigene Position immer in der Mittw
cl_radar_scale "0.3"                   // Rausgezoomt um ganze Map zu sehen
cl_hud_radar_scale "1.5"               // Etwas groesseres Radar
cl_radar_icon_scale_min "1"            // Icongroesse
cl_radar_rotate "1"                    // Radar rotieren / nicht immer eingenordet. Falls das Radar sich bei Euch nicht in
                                       // Blickrichtung mitdrehen soll einfach 0 statt 1 setzen
cl_radar_square_with_scoreboard "1"    // Eckiges Radar wenn Scoreboard gezeigt wird
```

## **Crosshair Einstellungen**

Optional:
Falls Ihr mit verschiedenen Fadenkreuzen experimentieren wollt könnt Ihr das hier (https://tools.dathost.net/) machen und was immer dort herauskommt einfach in die autoexec.cfg reinkopieren.

## **Schadensinformationen**

Optional:
Mit diesen drei Zeilen bekommt Ihr nach der Runde oben link Informationen über den erlittenen und verteilten Schaden in dieser Runde eingeblendet. Und zwar nicht nur für denjenigen der Euch getötet hat sondern für alle.

```
con_filter_text "Damage"
con_filter_text_out "Player:"
con_filter_enable "2"
```

## Soz Server

Empfohlen:
Um schnell auf den soz Server zu kommen lohnt sich:

    alias "soz" "connect soz-clan.de;password unserpasswort"

Natuerlich sollter ihr unserpasswort durch das richtige Passwort ersetzen ;)
Dann müsst Ihr nur noch soz in die Console tippen und joint dem soz Server.

## **Maus**

Empfohlen:
Zumindest 

    m_rawinput "1"
    
solltet Ihr setzen. Das hindert Windows daran an der Mausbeschleunigung rumzufummeln und sorgt dafür das jede Mausbewegung direkt und ungefiltert im Spiel ankommt.

Optional:
Zusätzlich könnt Ihr über die Config auch Eurer Sensitivity einstellen. Damit überschreibt Ihr was immer Ihr im Spiel über Menü konfiguriert. Guckt also vorher Euren Wert im Spiel nach und setzt Ihn erst dann über die Config:

    sensitivity "0.28"

## **Tastatur**

Jetzt zum größten Thema....Tastenbelegung. Ihr solltet Euch grundsätzlich entscheiden ob Ihr alle Tasten über die Config belegen wollt oder wie bisher größtenteils im Spiel im Menü. Ich mache Ersteres, aber das ist wohl Geschmackssache. **Nur** wenn Ihr die komplette Belegung in der autoexec.cfg machen wollt fügt bitte folgendes for irgendeinem "bind" Befehl in Eure Config ein:

    unbind all

Anschließend könnt Ihr mit

    bind "KEY" "BEFEHL"
    
alle Tasten so belegen wie Ihr wollt. 

    bind "w" "+forward"

sorgt beispielsweise dafür das Ihr mit W vorwärts lauft.

Falls Ihr alle Tasten über die autoexec.cfg konfigurieren wollt guckt Euch das am Besten in meiner Config unten ab. Im folgenden werde ich auf ein paar Spezialitäten eingehen:

### +cl_show_team_equipment

Um die eigenen Teammitglieder auch durch die Wände sehen zu können empfiehlt sich das grundsätzliche aktivieren von +cl_show_team_equipment. Das geht leider nicht ganz so einfach, das es ein Toggle ist. Hier ist das Snippet das Ihr in Eure Config kopieren müßt:
```
alias +showinv "+cl_show_team_equipment" 
alias -showinv ""
alias +hideinv "-cl_show_team_equipment" 
alias -hideinv ""

bind "TAB"    "+showscores; +showinv"
```

Sobald Ihr nun einmal TAB gedrückt habt im Spiel ist +cl_show_team_equipment dauerhaft an. Falls Ihr das in bestimmten Situationen auch wieder ausschalten wollt könnt ihr zusätzlich 

    bind "p"      "+hideinv"
    
machen.

### Messer Schnellwechsel

Optional: Falls Ihr viel mit AWP spielt empfohlen, aber auch sonst hilfreich
```
alias "+knife"             "slot3"
alias "-knife"             "lastinv"

bind "MOUSE4" "+knife"
```

Wenn ich Maustaste 4 drücke wechsel ich aufs Messer und sobald ich die Taste wieder loslasse zurück auf die letzte Waffe. Anwendungsfälle:
1. Nach einem AWP Schuss geht die Waffe direkt wieder in die letzte Zoom Stufe. Das ist meist lästig, da man schnell seine Posi ändern will. Daher klickt man direkt nach dem Schuss einmal schnell die Taste. Dadurch wechseln man für den Bruchteil einer Sekunde aufs Messer und direkt zuück zur AWP, die dann aber nicht mehr reingezommt ist.
2. Man will kurz aufs Messer wechseln um schneller zu laufen. Statt 3 fürs Messer und dann zwei Sekunden später wieder 1 oder 2 für die Waffe zu drücken hält man für die Zeit einfach kurz die Maustaste gedrückt.

Man kann das ganze natürlich auf eine beliebige andere Taste legen.

### Blutflecken entfernen

Mineralwasser und ein weiches Tuch.....nein quatsch.

    r_cleardecals
    
ist ein Konsolenkommando das Blutflecken, Einschusslöcher etc. entfernt. Hilft oft dabei weiter entfernte Gegner vor Blutflecken oder ähnlichem zu erkennen. Da Ihr das nicht immer in die Konsole tippen wollt empfehle ich das auch auf eine Taste zu legen. Entweder auf eine separate:

    bind "j" "r_cleardecals"
    
oder wie ich zum Beispiel auf die gleiche Taste wie ducken:

    bind "CTRL" "+duck; r_cleardecals"
    
### Easy DuckJump für unsere Bewegungslegastheniker

Optional:
Die folgenden drei Zeilen legen einen automatischen DuckJump auf die Leertaste. Statt manuell springen und dann ducken zu müssen reicht nur das drücken (und gedrückt halten) der Leertaste....oder einer beliebigen anderen Taste, je nachdem wie ihr wollt.
```
alias +LJ "+duck; +jump" 
alias -LJ "-duck; -jump"

bind "SPACE"         "+LJ"
```

### Lautstärke im Spiel ändern

Optional:
Durch drücken von i (oder irgendeiner anderen Taste) wird die Laustärke in 6 Stufen geändert. Von 0.0 (Mute) bis 1.0

```
bind "i"      "toggle volume 0.0 0.2 0.4 0.6 0.8 1.0"
```

### Bots steuern

Optional: Hier die beiden einzigen nützlichen Bot Kommandos.
* J für Position halten
* K für Folgen

```
bind "j"      "holdpos"        //Hold This Position
bind "k"      "followme"       //Follow Me
```

### Buyscripte

Optional:
Ich kaufe das meiste mit Hilfe von Buyscripten. Hier die Belegung meines Nummernpads. Das ganze kann man sich super auf https://tools.dathost.net/csgobindsgenerator zusammenstellen.

```
bind "KP_SLASH"      "buy hegrenade;"
bind "KP_MULTIPLY"   "buy flashbang;"
bind "KP_MINUS"      "buy incgrenade;buy molotov;"
bind "KP_HOME"       "buy p250;"
bind "KP_UPARROW"    "buy tec9; buy fiveseven;" 
bind "KP_PGUP"       "buy deagle;"
bind "KP_PLUS"       "buy smokegrenade;"
bind "kp_LEFTARROW"  "buy mac10; buy mp9;"
bind "KP_5"          "buy mp7;"
bind "KP_RIGHTARROW" "buy p90;"
bind "KP_END"        "buy famas;buy sg556;"
bind "KP_DOWNARROW"  "buy m4a1;buy ak47;"
bind "KP_PGDN"       "buy awp;"
bind "KP_ENTER"      "buy defuser;"
bind "KP_INS"        "buy vesthelm;"
bind "KP_DEL"        "buy vest;"
```

## **Andys config**
Bitte nicht einfach eins zu eins kopieren. Die komplette Tastaturbelegung wird dann bei Euch auf meine geändert. Aber zum Abgucken von einzelnen Sachen habe ich die mal hier reinkopiert:

```
// ****************
// General settings
// ****************
cl_forcepreload "1"  // Alle Ressourcen vor dem Starten einer Map laden. Verhindert Verzoegerungen beim Spielen
cl_showpos "0"       // Position und Geschwindigkeit nicht anzeigen
cl_cmdrate 128
cl_updaterate 128 
net_graph "1"        // FPS / UPS und andere Performance Infos anzeigen
net_graphproportionalfont "0"
cl_autowepswitch "0" // Autowechseln der Waffen beim Einsammeln ausschalten
voice_enable "1"     // Voice Chat an
developer "1"        // Entwicklerkonsole aktiv

// ******************
// Nach dem Tod wird
// oben links eine
// Damagestatistik 
// angezeigt
// ******************
con_filter_text "Damage"
con_filter_text_out "Player:"
con_filter_enable "2"

// ****************
// Mouse settings
// Raw Input statt
// komischer Filter
// ****************
m_rawinput "1"
sensitivity "0.28"

// **************
// Radar settings
// **************
cl_radar_always_centered "1"           // Eigene Position immer in der Mittw
cl_radar_scale "0.3"                   // Rausgezoomt
cl_hud_radar_scale "1.5"               // Groesseres Radar
cl_radar_icon_scale_min "1"            // Icongroesse
cl_radar_rotate "1"                    // Radar rotieren / nicht immer eingenordet
cl_radar_square_with_scoreboard "1"    // Eckiges Radar wenn Scoreboard gezeigt wird

// ******************
// Crosshair settings
// Copy Paste von https://tools.dathost.net/
// ******************
cl_crosshairalpha "150"
cl_crosshaircolor "5"
cl_crosshaircolor_b "0"
cl_crosshaircolor_r "0"
cl_crosshaircolor_g "255"
cl_crosshairdot "1"
cl_crosshairgap "2"
cl_crosshairsize "8"
cl_crosshairstyle "4"
cl_crosshairusealpha "1"
cl_crosshairthickness "1"
cl_fixedcrosshairgap "0"
cl_crosshair_outlinethickness "1"
cl_crosshair_drawoutline "1"

// *******
// Aliases
// *******
alias "soz" "connect soz-clan.de;password unserpasswort"

// Alias fuer Schnelles wechseln auf MEsser und zurueck zur Waffe
alias "+knife"             "slot3"
alias "-knife"             "lastinv"

// Alias fuer Immer Duckjump
alias +LJ "+duck; +jump" 
alias -LJ "-duck; -jump"

// Alias fuer Aktivieren/Deaktivieren von TeamEquipment
alias +showinv "+cl_show_team_equipment" 
alias -showinv ""
alias +hideinv "-cl_show_team_equipment" 
alias -hideinv ""

// ****************
// Control settings
// ****************
// Alle Tastenbelegungen loeschen
unbindall

// Movement
// ********
bind "w"             "+forward" // Vorwaerts
bind "s"             "+back"    // Rueckwaerts
bind "a"             "+moveleft" // Strafe left
bind "d"             "+moveright" // Strafe right

bind "MWHEELDOWN"    "+jump" // Springen
bind "MWHEELUP"      "+jump" // Springen

bind "SHIFT"         "+speed" // Schleichen
bind "SPACE"         "+LJ"  // DuckJump
bind "CTRL"          "+duck; r_cleardecals" // Ducken und Blutflecken, Einschussloecher etc entfernen

// Weapon selection
// ================
bind "1" "slot1"      // Waffenslots 1 - 10
bind "2" "slot2"
bind "3" "slot3"
bind "4" "slot4"
bind "5" "slot5"
bind "6" "slot6"
bind "7" "slot7"
bind "8" "slot8"
bind "9" "slot9"
bind "0" "slot10"
bind "q" "slot12"    // Healthshot fue Danger Zone

bind "y" "invprev"   // Durch Waffen / Equipment durchwechseln (zurueck)
bind "h" "invnext"   // Durch Waffen / Equipment durchwechseln (vor)

// Granaten auswaehlen
bind "z" "use weapon_knife;use weapon_hegrenade"
bind "x" "use weapon_knife;use weapon_flashbang"
bind "f" "use weapon_knife;use weapon_incgrenade; use weapon_molotov"
bind "r" "use weapon_knife;use weapon_decoy"
bind "c" "use weapon_knife;use weapon_smokegrenade"

// Flashbang auf Maustaste
bind "MOUSE5" "use weapon_knife;use weapon_flashbang"
// Messer <-> Letzte Waffe Schnellwechsel
bind "MOUSE4" "+knife"

// Other actions
// =============
bind "MOUSE1" "+attack"     // Schiessen
bind "MOUSE2" "+attack2"    // Waffenmodus, kurzer Granatenwurf, etc....
bind "MOUSE3" "+reload"     // Nachladen
bind "g"      "drop"        // Waffe fallenlassen
bind "e"      "+use"        // Benutzen, Tuer, Bombe, ...
bind "i"      "toggle volume 0.0 0.2 0.4 0.6 0.8 1.0"   // Lautstaerke
bind "v"      "+lookatweapon"      // Waffe anschauen
bind "n"      "messagemode2"       // Team Message
bind "m"      "messagemode"        // Server message
bind "t"      "teammenu"           // Team waehlen
bind "`"      "toggleconsole"      // Console
bind "TAB"    "+showscores; +showinv" // TAB zeigt Scoreboard und aktiviert Team-Equipment
bind "p"      "+hideinv"              // 'P' deaktiviert Team-Equipment
bind "ESCAPE" "cancelselect"          // Escape
bind "ALT"    "+voicerecord"          // Ingame voice
bind "l"      "+spray_menu"           // Grafitti
bind "j"      "holdpos"        // Hold This Position
bind "k"      "followme"       // Follow Me

bind "DEL" "mute"
bind "PAUSE" "pause"
bind "F3" "autobuy"
bind "F4" "rebuy"
bind "F5" "jpeg"
bind "F6" "save quick"
bind "F7" "load quick"
bind "F10" "quit prompt"

// Buyscripte
// ==========
bind "b"             "buymenu"   // Kaufmenue
bind "KP_SLASH"      "buy hegrenade;"
bind "KP_MULTIPLY"   "buy flashbang;"
bind "KP_MINUS"      "buy incgrenade;buy molotov;"
bind "KP_HOME"       "buy p250;"
bind "KP_UPARROW"    "buy tec9; buy fiveseven;" 
bind "KP_PGUP"       "buy deagle;"
bind "KP_PLUS"       "buy smokegrenade;"
bind "kp_LEFTARROW"  "buy mac10; buy mp9;"
bind "KP_5"          "buy mp7;"
bind "KP_RIGHTARROW" "buy p90;"
bind "KP_END"        "buy famas;buy sg556;"
bind "KP_DOWNARROW"  "buy m4a1;buy ak47;"
bind "KP_PGDN"       "buy awp;"
bind "KP_ENTER"      "buy defuser;"
bind "KP_INS"        "buy vesthelm;"
bind "KP_DEL"        "buy vest;"

```
